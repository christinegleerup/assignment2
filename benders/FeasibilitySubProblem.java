/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 
//Authors: Christine Gleerup-Morch and Petr Vaněk
//Applied Operations Research course at KU
//2019


package benders;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
import problems.UnitCommitmentProblem;


public class FeasibilitySubProblem {

    // First we set all of our desitions varibles for the feasibility problem, the slack varibles, the constrains and the model: 
    
    private final IloCplex model;
    private final IloNumVar c[][];
    private final IloNumVar l[];
    private final IloNumVar p[][];
    private final IloNumVar v1[][];
    private final IloNumVar v2p[];
    private final IloNumVar v2n[];
    private final IloNumVar v3[][];
    private final IloNumVar v4[][];
    private final IloNumVar v5[][];
    private final IloNumVar v6[][];
    private final UnitCommitmentProblem ucp;
    private final IloRange Constraints1b[][];
    private final IloRange Constraints1e[];
    private final IloRange Constraints1f[][];
    private final IloRange Constraints1g[][];
    private final IloRange Constraints1h[][];
    private final IloRange Constraints1i[][];
    
    // Now we create the submodel which reprecent our feasubility problem:        
    
     public FeasibilitySubProblem(UnitCommitmentProblem ucp, double U[][]) throws IloException {
         this.ucp = ucp;
         
         // Our model needs a IloCplex object:
         this.model = new IloCplex();
         
         // Creates the decision variables
         
         //c_gt
        c = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                c[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
            }
        }
        
        // l_t 
        l = new IloNumVar[ucp.getnHours()];
        for(int t = 1; t <= ucp.getnHours(); t++){
            l[t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
        }
        
        // p_gt 
        p = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                p[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
            }
        }
        
        // Creates the slack varibles one for each constrain except for the constrain which has
        // a equal sign. This constrain should have two slack varibles asigned. 
        
        this.v1 = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                v1[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
            }
        }
        
        this.v2p = new IloNumVar[ucp.getnHours()];
            for(int t = 1; t <= ucp.getnHours(); t++){
                v2p[t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
            }
        this.v2n = new IloNumVar[ucp.getnHours()];
            for(int t = 1; t <= ucp.getnHours(); t++){
                v2n[t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
            }    
     
        this.v3 = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                v3[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
            }
        }
        
        this.v4 = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                v4[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
            }
        }
        
        this.v5 = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                v5[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
            }
        }
        
        this.v6 = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                v6[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
            }
        }
        
        
        // Creates the objective function
        IloLinearNumExpr obj = model.linearNumExpr();
        
        // Here we add all the slack varibles to the obejct function:
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                obj.addTerm(1,v1[g-1][t-1]);
            }
        } 
        
        for(int t = 1; t <= ucp.getnHours(); t++){
         obj.addTerm(1,v2p[t-1]);
        }
        
        for(int t = 1; t <= ucp.getnHours(); t++){
         obj.addTerm(1,v2n[t-1]);
        }
       
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                obj.addTerm(1,v3[g-1][t-1]);
            }
        }
        
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                obj.addTerm(1,v4[g-1][t-1]);
            }
        }
        
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                obj.addTerm(1,v5[g-1][t-1]);
            }
        }
        
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                obj.addTerm(1,v6[g-1][t-1]);
            }
        }
       
        // We say to cplex that we want to minimize the objective function:
        model.addMinimize(obj);
       
        // We creates the constraints which will also contain the slackvaribles: 
        
        // Constraints (1b)
        // Note that we have added a if statement. This statement says that is the hour is largere than 1 then this constrains right hand side vil
        // be C_g^S(u_{g,t}-u{g,t-1}) which in the code correspond to t-1 and t-2. Else if we don't forfill that the hour is largere than 1
        // then the right hand side of this constrain will be C_g^S(u_g,t) whis in the code will be t-1. 
        
        this.Constraints1b = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1, c[g-1][t-1]);
                lhs.addTerm(1, v1[g-1][t-1]);
                if(t > 1){                 
                    Constraints1b[g-1][t-1] = model.addGe(lhs,ucp.getStartUpCost(g)*(U[g-1][t-1]-U[g-1][t-2]));
                
                 }else{
                  Constraints1b[g-1][t-1] = model.addGe(lhs,ucp.getStartUpCost(g)*(U[g-1][t-1]));
                }  
            }
        }
            
            // Constraints (1e)
            this.Constraints1e = new IloRange[ucp.getnHours()];
            for(int t = 1; t <= ucp.getnHours(); t++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                lhs.addTerm(1, p[g-1][t-1]);
            }
                lhs.addTerm(1, l[t-1]);
                lhs.addTerm(1, v2p[t-1]);
                lhs.addTerm(-1, v2n[t-1]);
                Constraints1e[t-1] = model.addEq(lhs, ucp.getDemand(t));
        }
            
             // Constraints (1f) 
        this.Constraints1f = new IloRange[ucp.getnGenerators()][ucp.getnHours()];     
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                lhs.addTerm(1,v3[g-1][t-1]);
                Constraints1f[g-1][t-1] = model.addGe(lhs,ucp.getMinProduction(g)*U[g-1][t-1]);
            }
        }
            
        // Constraints (1g)
        this.Constraints1g = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
        
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                lhs.addTerm(-1, v4[g-1][t-1]);
                Constraints1g[g-1][t-1] = model.addLe(lhs,ucp.getMaxProduction(g)*U[g-1][t-1]);
            }
        }    
        
        // Constraints (1h)
        this.Constraints1h = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                if(t > 1){
                    lhs.addTerm(-1,p[g-1][t-2]);
                }
                lhs.addTerm(-1, v5[g-1][t-1]);
                Constraints1h[g-1][t-1] = model.addLe(lhs,ucp.getRampUp(g));
            }
        }
        
        
        // Constraints (1i)
        
        this.Constraints1i = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
        
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                if(t > 1){
                    lhs.addTerm(1,p[g-1][t-2]);
                }
                lhs.addTerm(-1,p[g-1][t-1]);
                lhs.addTerm(-1, v6[g-1][t-1]);
                Constraints1i[g-1][t-1] = model.addLe(lhs, ucp.getRampDown(g));
            }
        }
         
        
     }
     
     // We solve the feasibility problem:
     //@throws IloException 

    public void solve() throws IloException{
        model.setOut(null);
        model.solve();
    }
    
    // @return the objective value
    // @throws IloException 


    public double getObjective() throws IloException{
        return model.getObjValue();
    }
    
     
    
    
    // We create the elements to the feasibility cuts
     
   // First we create the constant term which will be a part of the feasibility cut.
   // The constant cut will contain all the constant righthand sides form the above constains 
   //(all the terms which not contain u) multipled with the problems dual varibles. 
    
     //@return the constant of the cut
     //@throws IloException 
    
    public double getCutConstant() throws IloException{
        double constant = 0;
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                constant = constant + model.getDual(Constraints1e[t-1]) * ucp.getDemand(t)
                        + model.getDual(Constraints1h[g-1][t-1]) * ucp.getRampUp(g)
                        + model.getDual(Constraints1i[g-1][t-1]) * ucp.getRampDown(g);
            }
        }
        return constant;
    }
    
     // Second we create the linear cut term which should be contained in the feasibility cut. 
   // The linear term are made out of all the terms form the above constrains which include the u term times the 
   // dual varibles form this problem. 
   // Note that u is now a constant in the feasibility problem. u is a constant in problem because
   // becouse we get the value for u when solving the masterproblem. 
     // Not that we have added a if statement. This stament will only run if the current hour is largere than 1.

    
    public IloLinearNumExpr getCutLinearTerm(IloIntVar u[][]) throws IloException{
        // Define our linear variable 
        IloLinearNumExpr cutTerm = model.linearNumExpr();
        for(int g = 1; g<= ucp.getnGenerators(); g++){
            for(int t = 1; t<= ucp.getnHours(); t++){
                
                cutTerm.addTerm(model.getDual(Constraints1b[g-1][t-1])*ucp.getStartUpCost(g), u[g-1][t-1]);
                if(t > 1){
                    cutTerm.addTerm(model.getDual(Constraints1b[g-1][t-1])*(-ucp.getStartUpCost(g)), u[g-1][t-2]);
                }
                cutTerm.addTerm(model.getDual(Constraints1f[g-1][t-1])*ucp.getMinProduction(g), u[g-1][t-1]);
                cutTerm.addTerm(model.getDual(Constraints1g[g-1][t-1])*ucp.getMaxProduction(g), u[g-1][t-1]);
            }
        }
        return cutTerm;
    }
    
     
    public void end(){
        model.end();
    }  
    
}
