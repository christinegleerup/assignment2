/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 
//Authors: Christine Gleerup-Morch and Petr Vaněk
//Applied Operations Research course at KU
//2019
package benders;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
import problems.UnitCommitmentProblem;


public class MasterProblem {
    
    // We are defining which elements we are going to have in our masterproblem. 
    // The complicated varible is u. 
    
    private final IloCplex model;
    private final IloNumVar u[][];
    private final IloNumVar phi;
    private final UnitCommitmentProblem ucp;
    
    // We are now creating the masterproblem 
    //@param ucp
    //@throws ilog.concert.IloException
    
    public MasterProblem(UnitCommitmentProblem ucp) throws IloException{
        this.ucp = ucp;
        
        // Our model needs a IloCplex object:
        this.model = new IloCplex();  
        
        // Creates the decision variables which is the complicated varible u
        this.u = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        
        // Assigns a value for each position in the array:
        for(int g = 1; g<= ucp.getnGenerators(); g++){
            for( int t = 1; t<= ucp.getnHours(); t++ ){
                u[g-1][t-1] = model.boolVar();
            } 
        }
        
        // We define the other part of the object function for the master problem which is phi
        this.phi = model.numVar(0, Double.POSITIVE_INFINITY,"phi");
        
        
        // We create the object function: 
        IloLinearNumExpr obj = model.linearNumExpr();
        
        // Adds terms to the objectfunction: 
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){ 
                obj.addTerm(ucp.getCommitmentCost(g), u[g-1][t-1]);
            }
        }
        // Adds phi to the object function:
        obj.addTerm(1, phi);
        
        // We want to minimize the objective function of the master problem:
        model.addMinimize(obj);
    
        // Creates the constraints
        
        // Constraints (1c)
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                // We loop over t'
                for(int tt = t; tt <= ucp.getMinOnTimeAtT(g, t); tt++){
                    lhs.addTerm(1, u[g-1][tt-1]);
                    lhs.addTerm(-1, u[g-1][t-1]);
                    if(t > 1){
                        lhs.addTerm(1, u[g-1][t-2]);
                    }
                }
                model.addGe(lhs,0);
            }
        }
        
        // Constraints (1d)
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                // We loop over t'
                // We add one every time we loop. We save this sum in a constant
                int constant = 0;
                for(int tt = t; tt <= ucp.getMinOffTimeAtT(g, t); tt++){
                    constant++;
                    lhs.addTerm(-1, u[g-1][tt-1]);
                    lhs.addTerm(1, u[g-1][t-1]);
                    if(t > 1){
                        lhs.addTerm(-1, u[g-1][t-2]);
                    }
                }
                model.addGe(lhs,constant);
            }
        }
        
    }
    

    // We solve the master problem
    // @throws IloException 
     
    public void solve() throws IloException{
        
        // When solving the problem we are going to use Callback. The Callback is create below:
        model.use(new MyCallback());
        
        model.solve();
    }
    
    
    // Defining the callback use for solving the problem. The actions inside the callback will be
    // runed through every time we reach an integer node:
    
    private class MyCallback extends IloCplex.LazyConstraintCallback{
        
        public MyCallback() {
        }
    
        @Override
        protected void main() throws IloException {
            // The solution to U and phi at every intger node:
            double[][] U = getU();
            double Phi = getPhi();
            
            // For the feasibility subproblem
            
            // Creating and solving a feasibility subproblem 
            FeasibilitySubProblem usp = new FeasibilitySubProblem(ucp,U);
            usp.solve();
            double uspObjective = usp.getObjective();
            
            // We check if the subproblem problem is feasible which is when the objective value is 0.
            System.out.println("USP "+uspObjective);
            if(uspObjective >= 0+1e-9){
            
                // If the objective is not 0 then the subproblem is not feasible and 
                // we need to add the feasibility cut to the master problem

                System.out.println("Generating feasibility cut");
                       
                // Getting the constant and linear term form the feasibility subproblem
               
                double constantTerm = usp.getCutConstant();
                IloLinearNumExpr linearTerm = usp.getCutLinearTerm( (IloIntVar[][]) u);
                
                
                // Now we are putting together the feasibility cut  linearTerm <= -constant
                // and add the cut to the problem
                
                IloRange cut = model.le(linearTerm, -constantTerm);
                add(cut);

            }else{
                
                // In this case the subproblem is feasible and therefore we don't add a feasiblity cut
                // instead we are continuing and cheacking for optimality 
                
                // Creating and solving the optimality subproblem:
                OptimalitySubProblem osp = new OptimalitySubProblem(ucp,U);
                osp.solve();
                double ospObjective = osp.getObjective();
                
                //  We test if the solution is optimal:
                System.out.println("Phi "+Phi+ " OSP "+ospObjective );
                if(Phi >= ospObjective - 1e-9){
                    // If this statement is forfilled the current node is optimal and we don't add the optimality cut. 

                    System.out.println("The current node is optimal");
                }else{
                    
                    // In this case the current node is not optimal and therefore we need to add
                    // an optimality cut.
 
                    System.out.println("Generating optimality cut");
                    
                    // Getting the constant and linear term form the optimallity subproblem
                    
                    double cutConstantTerm = osp.getCutConstant();
                    IloLinearNumExpr cutTerm = osp.getCutLinearTerm(u);
                    
                    // Now we can create the optimality cut which is -phi * cutTerm <= -cutConstantTerm
                    // and then we are adding this cut to the model.
                    
                    cutTerm.addTerm(-1, phi);
                    // and generate and add a cut. 
                    IloRange cut = model.le(cutTerm, -cutConstantTerm);
                    add(cut);
                }
            }
        }
    

        // @return the value of U form the current solution
        // @throws IloException 
        
        public double[][] getU() throws IloException{
           double U[][] = new double[ucp.getnGenerators()][ucp.getnHours()];
           for(int g = 1; g<= ucp.getnGenerators() ;g++){
               for(int t = 1; t<= ucp.getnHours(); t++){
                   U[g-1][t-1] = getValue(u[g-1][t-1]);
               }
           }  
           return U;
       }
       
        // Create a metode to get the Phi 
        
        public double getPhi() throws IloException{
           return getValue(phi);
        }
        
        
     private IloRange model(int i, double d) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }   
        
        
     // Create a metode to get the objective value  
     
        public double getObjective() throws IloException{
        return model.getObjValue();
    }
       
    // We print value for each U, this value should end up being either 0 or 1 because it's a binary varible
    
        public void printSolution() throws IloException{
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t<=ucp.getnHours(); t++){
                System.out.println("U_"+g+""+t+" = "+model.getValue((u[g-1][t-1])));
            }
        }
    }
        
        
        
        public void end(){
        model.end();
    }
        
       
}    
